const mongoose = require('mongoose');

mongoose.connect(process.env.MONGODB_URI, () => {
    console.log("connected to database");
});